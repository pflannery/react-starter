# React Starter Template

To get started

- git clone https://gitlab.com/pflannery/react-starter
- ```npm install```
- ```npm start```

In another terminal

- ```npm run watch```
- Make your changes


Build Tasks

```yaml
- start # Runs a local web server at http://localhost:[port]

- build # Compiles everything and places the output to the ./dist folder

- compile # Compiles the javascript

- watch # Watch the source files for changes

- clean # Deletes the ./dist folder

- teardown # Deletes the ./dist folder and ./node_modules folder
```