export default props => (
  <div>
    <header>{props.header}</header>
    <main>{props.children}</main>
  </div>
)