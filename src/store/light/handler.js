import * as StoreActions from './actions.js'
import defaultState from './default.js'

// creates the message handler for talking to the store
// in this example we're keeping the state immutable
export default function messageHandler(state = defaultState, event) {
  switch (event.type) {
    case StoreActions.LIGHT_ON:
      return Object.assign({}, state, { on: true })

    case StoreActions.LIGHT_OFF:
      return Object.assign({}, state, { on: false })

    case StoreActions.TOGGLE_LIGHT:
      return Object.assign({}, state, { on: !state.on })

    case StoreActions.SET_LIGHT_COLOUR:
      return Object.assign({}, state, { colour: event.colour })

    default:
      return state
  }
}
