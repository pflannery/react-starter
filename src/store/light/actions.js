// action types
export const TOGGLE_LIGHT = 'TOGGLE_LIGHT'
export const LIGHT_ON = 'LIGHT_ON'
export const LIGHT_OFF = 'LIGHT_OFF'
export const SET_LIGHT_COLOUR = 'SET_LIGHT_COLOUR'