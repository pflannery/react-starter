import * as Actions from './actions.js'

export function lightService(dispatch) {
  return {
    togglePower: () => dispatch({ type: Actions.TOGGLE_LIGHT }),
    setColour: colour => dispatch({ type: Actions.SET_LIGHT_COLOUR, colour })
  }
}