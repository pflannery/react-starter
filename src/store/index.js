import light from './light/handler.js'

const store = Redux.createStore(
  Redux.combineReducers({
    light
  })
)

export default store
export { lightService } from './light/service.js'