const { Route, BrowserRouter } = ReactRouterDOM

import IndexPage from 'pages/index.js'
import AboutPage from 'pages/about.js'

export default 
  <BrowserRouter>
    <div>
      <Route exact path="/" component={IndexPage} />
      <Route path="/about" component={AboutPage} />
    </div>
  </BrowserRouter >