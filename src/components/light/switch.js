import { lightService } from 'store'

@ReactRedux.connect(
  store => ({ light: store.light }),
  dispatch => ({ lightService: lightService(dispatch) })
)
export default class LightSwitchComponent extends React.Component {

  render() {
    const props = this.props
    const toggleClassNames = props.light.on ?
      'btn btn-danger active' :
      'btn btn-success active'

    const toggleText = props.light.on ?
      'Turn off' :
      'Turn on'

    return <div>
      <button type="button" className={toggleClassNames} onClick={props.lightService.togglePower}>{toggleText}</button>
    </div>
  }

}