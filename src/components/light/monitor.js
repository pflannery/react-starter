@ReactRedux.connect(
  store => ({ light: store.light })
)
export default class LightMonitorComponent extends React.Component {

  render() {
    const props = this.props
    const attr = {
      className: 'light',
      style: {
        backgroundColor: props.light.on ? props.light.colour : ''
      }
    }
    const render =
      <div>
        <img src="assets/svg/light-bulb.svg" {...attr} />
      </div>

    return render
  }

}