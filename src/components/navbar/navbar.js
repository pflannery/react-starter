import LightMonitorComponent from 'components/light/monitor.js'
const { NavLink } = ReactRouterDOM

const menu = [
  { title: 'Index', path: '/' },
  { title: 'About', path: '/about' }
]

function NavList() {
  const MenuItems = menu.map(entry =>
    <li key={entry.path} className="nav-item">
      <NavLink exact className="nav-link" to={entry.path} activeClassName="active">
        {entry.title}
      </NavLink>
    </li>
  )

  return (
    <ul className="navbar-nav mr-auto">
      {MenuItems}
    </ul>
  )
}

export default
  <nav className="navbar navbar-expand-md navbar-dark bg-dark">
    <a className="navbar-brand" href="#">React Starter</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarCollapse">
      <NavList />
      <LightMonitorComponent />
    </div>
  </nav>
