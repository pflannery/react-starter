import MasterLayout from 'layouts/master.js'
import NavBarComponent from 'components/navbar/navbar.js'
import LightSwitchComponent from 'components/light/switch.js'

export default props => (
  <MasterLayout header={NavBarComponent}>
    <section className="jumbotron text-center">
      <div className="container">
        <h1 className="jumbotron-heading">About</h1>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
          Iure temporibus eligendi beatae, libero maiores, consequuntur asperiores aperiam impedit provident nobis voluptate suscipit dignissimos ipsum illo praesentium officia dolores amet quae? 
          Totam voluptas voluptatem, quis consequuntur. Vitae commodi, aliquid fuga deleniti!
        </p>
      </div>
    </section>
  </MasterLayout>
)