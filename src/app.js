import RouterComponent from './router.js'
import theStore from 'store'

const { Provider } = ReactRedux

const StoreProviderComponent =
  <Provider store={theStore}>
    {RouterComponent}
  </Provider>

ReactDOM.render(
  StoreProviderComponent,
  document.getElementById('react-root')
)